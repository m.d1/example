package com.credit7.android;

import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.installreferrer.api.InstallReferrerClient;
import com.android.installreferrer.api.InstallReferrerStateListener;
import com.facebook.FacebookSdk;
import com.facebook.applinks.AppLinkData;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import java.util.HashMap;
import java.util.Map;

public class SplashScreenActivity extends AppCompatActivity {
    public static final String INITIAL_PARAMS = "SplashScreen.INITIAL_PARAMS";
    public static final String START_URL = "SplashScreen.START_URL";
    public static final String START_PARAMS = "SplashScreen.START_PARAMS";
    public static final String ORGANIC_PARAMS = "utm_source=google-play&utm_medium=organic";
    private String startUrl;
    private String startParams;
    private InstallReferrerClient referrerClient;
    private Bundle initialParams;
    private FirebaseRemoteConfig firebaseRemoteConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(3600)
                .build();
        firebaseRemoteConfig.setDefaultsAsync(R.xml.remote_config_defaults);
        firebaseRemoteConfig.setConfigSettingsAsync(configSettings);
        initDefaultUrlParamsFromFirebase();
    }

    private void putParametersInInitialParams() {
        initialParams = new Bundle();
        initialParams.putString(START_URL, startUrl);
        initialParams.putString(START_PARAMS, startParams);
        startMainActivity();
    }

    private void startMainActivity() {
        Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
        intent.putExtra(INITIAL_PARAMS, initialParams);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

    private void getPlayStoreParamsFromGooglePlay() {
        referrerClient = InstallReferrerClient.newBuilder(this).build();
        referrerClient.startConnection(new InstallReferrerStateListener() {
            @Override
            public void onInstallReferrerSetupFinished(int responseCode) {
                switch (responseCode) {
                    case InstallReferrerClient.InstallReferrerResponse.OK:
                        try {
                            String refParams = referrerClient.getInstallReferrer().getInstallReferrer().replaceAll(ORGANIC_PARAMS,"");
                            if(refParams.contains("source")) {
                                startParams = refParams;
                            }
                            getFaceBookParams();

                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                        break;
                    case InstallReferrerClient.InstallReferrerResponse.FEATURE_NOT_SUPPORTED:
                    case InstallReferrerClient.InstallReferrerResponse.SERVICE_UNAVAILABLE:
                        putParametersInInitialParams();
                        break;
                }
                referrerClient.endConnection();
            }

            @Override
            public void onInstallReferrerServiceDisconnected() {
                putParametersInInitialParams();
            }
        });
    }

    private void getFaceBookParams() {
        FacebookSdk.setAutoInitEnabled(true);
        FacebookSdk.fullyInitialize();
        AppLinkData.fetchDeferredAppLinkData(getApplicationContext(), new AppLinkData.CompletionHandler() {
            @Override
            public void onDeferredAppLinkDataFetched(@Nullable AppLinkData appLinkData) {
                if (appLinkData != null && appLinkData.getTargetUri() != null
                        && appLinkData.getTargetUri().getAuthority() != null
                        && appLinkData.getTargetUri().getAuthority().contains("source")) {
                        startParams = appLinkData.getTargetUri().getAuthority();
                }
                putParametersInInitialParams();
            }
        });
    }

    private void initDefaultUrlParamsFromFirebase() {
        firebaseRemoteConfig.fetchAndActivate()
                .addOnCompleteListener(this, new OnCompleteListener<Boolean>() {
                    @Override
                    public void onComplete(@NonNull Task<Boolean> task) {
                        startUrl = firebaseRemoteConfig.getString("start_url");
                        startParams = firebaseRemoteConfig.getString("start_params");
                        getPlayStoreParamsFromGooglePlay();
                    }
                });
    }
}