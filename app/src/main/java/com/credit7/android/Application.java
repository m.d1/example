package com.credit7.android;

import com.onesignal.OneSignal;
import com.squareup.okhttp.OkHttpClient;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;

public class Application extends android.app.Application {
    private OkHttpClient client;

    @Override
    public void onCreate() {
        super.onCreate();
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
        YandexMetricaConfig config = YandexMetricaConfig.newConfigBuilder("32b97362-f272-4a4c-af05-5ae605916aa1").build();
        YandexMetrica.activate(getApplicationContext(), config);
        YandexMetrica.enableActivityAutoTracking(this);
        client = new OkHttpClient();
    }

    public OkHttpClient getClient(){
        return client;
    }
}
