package com.credit7.android;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private String url;
    private String urlParams;
    private WebView webView;
    private boolean isLoadingSuccess = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Toast.makeText(this, "Main Activity", Toast.LENGTH_LONG).show();
        init();
    }

    private void init() {
        webView = findViewById(R.id.mainWebView);
        CookieManager cookieManager = CookieManager.getInstance();
        CookieManager.setAcceptFileSchemeCookies(true);
        cookieManager.setAcceptThirdPartyCookies(webView, true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setDefaultTextEncodingName("utf-8");
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
                isLoadingSuccess = false;
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return super.shouldOverrideUrlLoading(view, request);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                isLoadingSuccess = true;
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Log.d(TAG, "onPageFinished: " + url);
                saveLastUrl(url);
            }
        });
        webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        Bundle params = getIntent().getBundleExtra(SplashScreenActivity.INITIAL_PARAMS);
        if (params != null && !params.isEmpty()) {
            urlParams = params.getString(SplashScreenActivity.START_PARAMS);
            url = params.getString(SplashScreenActivity.START_URL);
        }
        webView.loadUrl(createUrlWithParams());
    }

    @Override
    public void onBackPressed() {
        if (webView != null && webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }

    }


    private String createUrlWithParams() {
        return getSharedPreferences("prefs", MODE_PRIVATE).getString("url", url + urlParams);

    }

    private void saveLastUrl(String url) {
        if (isLoadingSuccess) {
            SharedPreferences sharedPreferences = Objects.requireNonNull(getSharedPreferences("prefs", MODE_PRIVATE));
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("url", url);
            editor.apply();
        }
    }
}
